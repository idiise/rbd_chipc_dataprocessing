June 2020 CH update notes

For Burkina Faso, there was a descrepancy in the number of people in phase3-5, between the CILSS presentation "CH-mises jour juillet 2020" and country figures "BF-CH_Fiche de communication_Juillet 2020_def".  
We use the country office figures in the processed dataset.

For Nigeria, there was a descrepancy in the number of people in phase3-5 in Yobe State, between the CILSS presentation "CH-mises jour juillet 2020" and country figures "FINAL_Fiche-Nigeria_JUNE_ 2020". 
Also, in both the CILSS presentation and the country fiche - the figures for Gombe were accidentally doubled - the number of phase25 should be 97,419 and not  194,838